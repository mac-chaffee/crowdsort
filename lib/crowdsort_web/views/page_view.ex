defmodule CrowdsortWeb.PageView do
  use CrowdsortWeb, :view
  alias Crowdsort.{Repo, Chunk, Sortable}
  import Ecto.Query, only: [from: 2]
  require Logger

  @doc """
  Returns a random chunk id and the chunk data
  """
  def get_chunk_data(chunk_id) when is_nil(chunk_id) do
    query = from c in Chunk,
              where: c.sorted == false,
              order_by: fragment("RANDOM()"),
              limit: 1
    chunk = query |> Repo.one()
    if is_nil(chunk) do
      {nil, []}
    else
      {chunk.id, Map.get(chunk.data, "data")}
    end
  end

  # Returns a specific chunk id and the chunk data
  def get_chunk_data(chunk_id) do
    query = from c in Chunk,
              where: c.id == ^chunk_id
    chunk = query |> Repo.one()
    if is_nil(chunk) do
      {nil, []}
    else
      {chunk.id, Map.get(chunk.data, "data")}
    end
  end

  @doc """
  Returns a list of all sortables and their progress
  """
  def get_sortables_stats do
    # Get how many chunks are sorted for each sortable
    num_sorted_chunks = from s in Sortable,
              join: c in Chunk,
              on: c.sortable_id == s.id,
              where: c.sorted == true,
              group_by: s.id,
              select: %{id: s.id, total: count(c.id)}
    # Join the above query with the total num chunks for each sortable
    Repo.all(from s in Sortable,
              join: c in Chunk,
              on: c.sortable_id == s.id,
              left_join: sub in subquery(num_sorted_chunks),
              on: sub.id == s.id,
              order_by: s.id,
              group_by: [s.id, sub.total],
              select: {s.id, (type(sub.total, :float) / count(c.id)) * 100.0})
  end
end
