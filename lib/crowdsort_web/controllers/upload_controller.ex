defmodule CrowdsortWeb.UploadController do
  use CrowdsortWeb, :controller

  @doc """
  Save the POSTed changeset to a new Sortable, and redirect to track
  that new Sortable
  """
  def upload(conn, %{"data" => raw_data}) do
    case Sortable.create_chunks(raw_data) do
      {:ok, sortable_id} -> redirect(conn, to: "/track/#{sortable_id}")
      {:error, message} ->
        conn
        |> put_flash(:danger, message)
        |> render("upload.html")
    end
  end

  # Just render the template on the initial GET
  def upload(conn, _params) do
    render(conn, "upload.html")
  end

  @doc """
  Pass along the chunk_ids to monitor over channels
  """
  def track(conn, %{"sortable_id" => sortable_id}) do
    sortable_id = String.to_integer(sortable_id)
    query = from c in Chunk,
              select: c.id,
              where: c.sortable_id == ^sortable_id,
              order_by: c.id
    chunk_ids = Repo.all(query)
    render(conn, "track.html", chunk_ids: chunk_ids, sortable_id: sortable_id, token: get_csrf_token())
  end

  @doc """
  Get all the chunks, merge them together, and return them as a json array
  """
  def track_merge(conn, %{"sortable_id" => sortable_id}) do
    query = from c in Chunk,
                select: c.data,
                where: c.sortable_id == ^sortable_id
    all_data = Repo.all(query)
    json(conn, Sortable.merge_data(all_data))
  end
end
