defmodule CrowdsortWeb.PageController do
  use CrowdsortWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end

  def readyz(conn, _params) do
    conn
    |> put_resp_content_type("text/plain")
    |> send_resp(200, "")
  end

  def sort(conn, %{"chunk_id" => chunk_id}) do
    render conn, "sort.html", chunk_id: chunk_id
  end

  def sort(conn, _params) do
    render conn, "sort.html"
  end
end
