defmodule CrowdsortWeb.ChunkChannel do
  use Phoenix.Channel
  import Ecto.Query, only: [from: 2]
  alias Crowdsort.{Repo, Chunk}
  alias CrowdsortWeb.Presence
  require Logger

  @doc"""
  Set the chunk id and initial data to persist between messages
  """
  def join("chunk:" <> chunk_id, _message, socket) do
    %{"data" => initial_data} = Repo.get!(Chunk, chunk_id).data
    send(self(), :after_join)
    socket = socket
    |> assign(:id, chunk_id)
    |> assign(:data_list, initial_data)
    |> assign(:dirty, false)

    case Map.get(socket.assigns, :user_id) do
      uid when uid == "human" ->
        track_human(socket, uid)
      _ ->
        socket
    end

    {:ok, socket}
  end

  defp track_human(socket, uid) do
    # Kill any bots on the socket currently
    bot_pids(socket) |> Enum.map(&Process.exit(&1, :kill))
    Presence.track(socket, uid, %{})
  end

  # Starts a bot, but only if no other bot is running on the chunk
  # [{"bogobot", %{metas: [%{phx_ref: "F-fI4NXCfkXT7gAo"}]}}]
  @spec start_bot(module(), Phoenix.Socket, list()) :: Phoenix.Socket
  defp start_bot(bot_module, socket, params) do
    if length(bot_pids(socket)) == 0 do
      {:ok, pid} = bot_module.start_link(params)
      Presence.track(pid, socket.topic, serialize_pid(pid), %{})
    end
  end

  # Functions for managing bot pids, which must be serialized/deserialized
  # and checked when humans and other bots join
  defp bot_pids(socket) do
    socket
    |> Presence.list
    |> Enum.map(&elem(&1, 0))
    |> Enum.reject(fn x -> x == "human" end)
    |> Enum.map(&deserialize_pid/1)
  end

  defp serialize_pid(pid) do
    pid |> :erlang.pid_to_list |> to_string
  end

  defp deserialize_pid(pid_str) do
    pid_str |> to_charlist |> :erlang.list_to_pid
  end

  @doc"""
  Send the result of calc_sortedness as soon as someone joins,
  and handle Presence tracking
  """
  def handle_info(:after_join, socket) do
    push(socket, "sortedness", %{body: Chunk.calc_sortedness(socket.assigns.data_list)})
    push(socket, "presence_state", Presence.list(socket))
    {:noreply, socket}
  end

  # When a bot changes data, update it. Must be handle_info
  # since handle_in comes from clients exclusively
  def handle_info({:bot_chunk_update, new_data}, socket) do
    do_update(new_data, socket)
  end
  # Separate handler for when the bot finishes which triggers an immediate save.
  # Without this, data wouldn't be save until the "tracker" leaves the page.
  def handle_info({:bot_finished, sorted_data}, socket) do
    do_update(sorted_data, socket)
    save_chunk(socket, true)
    {:noreply, socket}
  end

  # When the human changes data, update it
  def handle_in("chunk_update", %{"body" => new_data}, socket) do
    do_update(new_data, socket)
  end
  # Spawn a bot on the current chunk when the frontend requests it
  def handle_in("start_bot", bot_type, socket) do
    case bot_type do
      "bogobot" ->
        start_bot(Crowdsort.BogoBot, socket, [socket.topic, socket.assigns.data_list])
        {:noreply, socket}
      "quickbot" ->
        start_bot(Crowdsort.QuickBot, socket, [socket.topic, socket.assigns.data_list])
        {:noreply, socket}
      _ ->
        {:reply, :error, socket}
    end
  end

  # On update (from either bots or humans), update the in-memory representation
  # calculate the level of sortedness, and broadcast that number
  defp do_update(new_data, socket) do
    socket = socket
    |> assign(:dirty, true)
    |> assign(:data_list, new_data)
    broadcast!(socket, "sortedness", %{body: Chunk.calc_sortedness(new_data)})
    {:noreply, socket}
  end

  def terminate(_reason, socket) do
    save_chunk(socket, socket.assigns.dirty)
    :ok
  end

  @doc"""
  Write out the in-memory representation to the database
  only if it's been changed (dirty)
  """
  def save_chunk(socket, dirty) when dirty do
    is_sorted = Chunk.calc_sortedness(socket.assigns.data_list) >= 1.0
    from(c in Chunk, where: c.id == ^socket.assigns.id)
    |> Repo.update_all(set: [data: %{"data" => socket.assigns.data_list}, sorted: is_sorted])
  end
  def save_chunk(_socket, dirty) when not dirty do
    :noop
  end
end
