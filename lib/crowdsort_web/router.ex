defmodule CrowdsortWeb.Router do
  use CrowdsortWeb, :router
  import Phoenix.LiveDashboard.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", CrowdsortWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/readyz", PageController, :readyz
    get "/sort", PageController, :sort
    get "/sort/:chunk_id", PageController, :sort
    get "/upload", UploadController, :upload
    post "/upload", UploadController, :upload
    get "/track/:sortable_id", UploadController, :track
    get "/track/:sortable_id/merge", UploadController, :track_merge
  end

  if Mix.env() == :dev do
    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: CrowdsortWeb.Telemetry, ecto_repos: [Crowdsort.Repo]
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", CrowdsortWeb do
  #   pipe_through :api
  # end
end
