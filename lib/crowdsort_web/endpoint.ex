defmodule CrowdsortWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :crowdsort

  socket "/socket", CrowdsortWeb.UserSocket

  # https://hexdocs.pm/phoenix_live_dashboard/Phoenix.LiveDashboard.html
  socket "/live", Phoenix.LiveView.Socket

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phoenix.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/", from: :crowdsort, gzip: true,
    only: ~w(assets images favicon.ico frankenstein.txt robots.txt)

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    socket "/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket
    plug Phoenix.LiveReloader
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId
  plug Plug.Logger

  # if Mix.env() == :prod, do: plug(CrowdsortWeb.ForceSSL)

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Jason

  plug Plug.MethodOverride
  plug Plug.Head

  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  plug Plug.Session,
    store: :cookie,
    key: "_crowdsort_key",
    signing_salt: "qGLsopD8"

  plug CrowdsortWeb.Router
end
