defmodule CrowdsortWeb.ForceSSL do
  @behaviour Plug

  @impl Plug
  def init(opts), do: opts

  @impl Plug
  def call(conn, _opts), do: Plug.SSL.call(conn, Plug.SSL.init(exclude: []))
end
