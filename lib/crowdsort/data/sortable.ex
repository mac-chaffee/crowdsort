defmodule Crowdsort.Sortable do
  use Ecto.Schema
  import Ecto.Changeset
  alias __MODULE__
  alias Crowdsort.{Chunk, Repo}
  require Logger

  schema "sortables" do
    has_many :chunks, Chunk
    timestamps()
  end

  @doc """
  Currently shouldn't hold anything but a collection of Chunks
  """
  def changeset(%Sortable{} = sortable, attrs) do
    sortable
    |> cast(attrs, [])
    |> validate_required([])
  end

  @doc """
  Given a list of lists, create a Sortable and its Chunks
  """
  def save_chunks(chunks) do
    sortable = Repo.insert!(%Sortable{})

    for chunk_data <- chunks do
      already_sorted? = Chunk.calc_sortedness(chunk_data) >= 1.0
      Repo.insert!(
        %Chunk{data: %{data: chunk_data}, sortable_id: sortable.id, sorted: already_sorted?}
      )
    end
    {:ok, sortable.id}
  end

  @doc """
  Given a list of strings (where each string is an element to sort),
  ensure the list isn't too long and that it doesn't contain naughty things
  """
  def validate(elements) when length(elements) > 1000 do
    raise "Too many elements to sort (>1000)"
  end

  def validate(elements) when length(elements) == 0 do
    raise "The dataset was empty (length = 0)"
  end

  def validate(elements) do
    config = Expletive.configure(
      blacklist: Expletive.Blacklist.english,
      # The blacklist can be a little overzealous IMO
      whitelist: ~w[murder suicide devil satanic eggplant flip kill slope]
    )
    for element <- elements do
      if is_bitstring(element) and Expletive.profane?(element, config) do
        raise "Element '#{element}' contains a blacklisted word. Sorry!"
      end
    end
    elements
  end

  @doc """
  Convert the string "data" into Chunk objects and save them, returning either
  {:ok, sortable_id} or {:error, message}
  """
  def create_chunks(data) do
    Logger.info "Creating chunks with raw data: #{data}"
    try do
      data
      |> String.trim()
      |> Jason.decode!()
      |> validate()
      |> Enum.chunk_every(10)
      |> save_chunks()
    rescue
      e in RuntimeError -> {:error, e.message}
      e in Jason.DecodeError -> {:error, "JSON Error at position #{e.position} in #{data}"}
    end
  end

  @doc """
  Given a list of maps [%{data: [1,2,3]}, ...] where each map is a sorted chunk,
  merge all chunks together into a single sorted list
  """
  def merge_data(all_data) do
    all_lists = Enum.map(all_data, &Map.get(&1, "data"))
    :lists.merge(all_lists)
  end
end
