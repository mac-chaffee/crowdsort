defmodule Crowdsort.Chunk do
  use Ecto.Schema
  import Ecto.Changeset
  alias __MODULE__

  schema "chunks" do
    field :data, :map
    field :sorted, :boolean, default: false
    belongs_to :sortable, Crowdsort.Sortable
    timestamps()
  end

  @doc """
  Holds a map (stored as JSONB) and a link to a Sortable
  """
  def changeset(%Chunk{} = chunk, attrs) do
    chunk
    |> cast(attrs, [:data, :sortable_id])
    |> validate_required([:data, :sortable_id])
  end

  @doc """
  Returns a new list with the element at old_idx moved to new_idx.
  Can be sped up by manually traversing the linked list if that's possible.
  """
  def move(data_list, old_idx, new_idx) do
    {val, new_list} = List.pop_at(data_list, old_idx)
    List.insert_at(new_list, new_idx, val)
  end

  @doc """
  Calculates the total inversions and divides that
  by n choose 2 (total possible inversions) to measure sortedness.
  """
  def calc_sortedness(list) do
    len = length(list)  # O(n)
    do_calc_sortedness(list, len)
  end
  defp do_calc_sortedness(_list, len) when len < 2, do: 1.0
  defp do_calc_sortedness(list, len) do
    num_inverted = combine(list, 2, len) |> Enum.count(&inverted?/1)  # O(n + nCk)
    # num / nCk, O(2n)
    num_inverted / (factorial(len) / (factorial(2) * factorial(len - 2)))
  end

  defp inverted?([a, b]), do: a >= b
  defp inverted?(_other), do: false

  @doc """
  Calculate the factorial of a number.
  Modified from https://gist.github.com/CMCDragonkai/7cd518eeb69be9fb6c32
  """
  def factorial(num), do: factorial(num, 1)
  defp factorial(num, _product) when num < 0, do: raise ArithmeticError
  defp factorial(num, product) when num <= 1, do: product
  defp factorial(num, product), do: factorial(num - 1, product * num)

  @doc """
  Get all combinations of k elements from a list.
  Stolen from https://github.com/seantanly/elixir-combination/blob/master/lib/combination.ex
  """
  def combine(list, k, list_length) when list_length < k do
    [list]
  end
  def combine(list, k, list_length) do
    do_combine(list, list_length, k, [], [])
  end

  defp do_combine(_list, _list_length, 0, _pick_acc, _acc), do: [[]]
  defp do_combine(list, _list_length, 1, _pick_acc, _acc), do: list |> Enum.map(&([&1])) # optimization
  defp do_combine(list, list_length, k, pick_acc, acc) do
    list
    |> Stream.unfold(fn [h | t] -> {{h, t}, t} end)
    |> Enum.take(list_length)
    |> Enum.reduce(acc, fn {x, sublist}, acc ->
      sublist_length = Enum.count(sublist)
      pick_acc_length = Enum.count(pick_acc)
      if k > pick_acc_length + 1 + sublist_length do
        acc # insufficient elements in sublist to generate new valid combinations
      else
        new_pick_acc = [x | pick_acc]
        new_pick_acc_length = pick_acc_length + 1
        case new_pick_acc_length do
          ^k -> [new_pick_acc | acc]
          _  -> do_combine(sublist, sublist_length, k, new_pick_acc, acc)
        end
      end
    end)
  end
end
