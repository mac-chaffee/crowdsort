defmodule Crowdsort.Repo do
  use Ecto.Repo,
    otp_app: :crowdsort,
    adapter: Ecto.Adapters.Postgres
end
