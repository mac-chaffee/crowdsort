defmodule Crowdsort.BogoBot do
  use Task, restart: :temporary
  require Logger

  def start_link([topic, data]) do
    Task.start_link(__MODULE__, :run, [topic, data])
  end

  @doc"""
  Run BogoSort over chunks in the sortable
  https://en.wikipedia.org/wiki/Bogosort
  """
  def run(topic, data) do
    bogo(topic, data)
  end

  @doc"""
  Recursive implementation of BogoSort
  """
  def bogo(topic, data) do
    :timer.sleep(1000)
    new_data = Enum.shuffle(data)
    Phoenix.PubSub.broadcast!(
      Crowdsort.PubSub,
      topic,
      {:bot_chunk_update, new_data}
    )
    if !is_sorted?(new_data) do
      bogo(topic, new_data)
    end
    Phoenix.PubSub.broadcast!(
      Crowdsort.PubSub,
      topic,
      {:bot_finished, new_data}
    )
  end

  # Ensure every element is less than the element that comes after it
  def is_sorted?(data) do
    elem(Enum.reduce(data, {true, nil}, &do_compare/2), 0)
  end

  # Compare two elements inside Enum.reduce
  def do_compare(cur, {acc, prev}) do {acc and (prev < cur), cur} end
end
