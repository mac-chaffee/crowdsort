defmodule Crowdsort.QuickBot do
  use Task
  require Logger

  def start_link([topic, data]) do
    Task.start_link(__MODULE__, :run, [topic, data])
  end

  @doc"""
  Run QuickSort over chunks in the sortable
  """
  def run(topic, data) do
    sorted_data = quicksort(topic, data, length(data))
    show_swaps(topic, sorted_data, length(sorted_data))
    Phoenix.PubSub.broadcast!(
      Crowdsort.PubSub,
      topic,
      {:bot_finished, sorted_data}
    )
  end

  @doc"""
  Implementation of QuickSort. History: I did implement my own translation of the
  quicksort algorithm, but it was super slow. So I borrowed this one:
  https://rosettacode.org/wiki/Sorting_algorithms/Quicksort#Elixir
  """
  def quicksort(_, data, _) when length(data) <= 1 do data end
  def quicksort(topic, [pivot_elem | data], len) do
      {lower, higher} = Enum.split_with(data, &(&1 < pivot_elem))
      show_swaps(topic, lower ++ [pivot_elem] ++ higher, len)
      quicksort(topic, lower, len) ++ [pivot_elem] ++ quicksort(topic, higher, len)
  end

  defp show_swaps(topic, data, len) do
    if length(data) == len do
      :timer.sleep(1000)
      Phoenix.PubSub.broadcast!(
        Crowdsort.PubSub,
        topic,
        {:bot_chunk_update, data}
      )
    end
    # Otherwise its too complicated, just don't show it :/
  end
end
