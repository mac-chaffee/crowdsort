# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :crowdsort,
  ecto_repos: [Crowdsort.Repo],
  generators: [timestamp_type: :utc_datetime]

# Configures the endpoint
config :crowdsort, CrowdsortWeb.Endpoint,
  url: [host: "localhost"],
  adapter: Bandit.PhoenixAdapter,
  render_errors: [view: CrowdsortWeb.ErrorView, accepts: ~w(html json)],
  pubsub_server: Crowdsort.PubSub,
  live_view: [signing_salt: "jbLmKQ+BB4+wQ/9NyLphYW49mz1dpV/Q"]

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.17.11",
  crowdsort: [
    args:
      ~w(js/app.js --bundle --target=es2017 --global-name=csjs --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$date $time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
# Don't load prod.exs since I replaced it with runtime.exs
#if Mix.env in [:dev, :test] do
import_config "#{config_env()}.exs"
#end
