import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :crowdsort, CrowdsortWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :crowdsort, Crowdsort.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "crowdsort",
  password: "XMLu5CsvSVWICOD1a3Iegw",
  database: "crowdsort_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
