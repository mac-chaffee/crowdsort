import {Socket, Presence} from "phoenix"

function getSocket(userID) {
  if (userID != "human" && userID != "tracker" && !userID.endsWith("bot")) {
    throw `userID ${userID} must be "human" or "tracker" or "*bot"`;
  }
  let socket = new Socket("/socket", {
    params: {user_id: userID}
  });
  socket.connect();
  return socket;
}

function connectToChunk(socket, chunkID, trackPresence) {
  // Join the channel with the given chunk id
  let channelName = "chunk:" + chunkID;
  let channel = socket.channel(channelName);
  channel.join()
    .receive("ok", resp => {})
    .receive("error", resp => { console.error("Unable to join", resp) });
  if (trackPresence) {
    let presence = new Presence(channel);
    return [channel, presence];
  }
  return channel;
}

export {getSocket, connectToChunk}
