// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"
import "phoenix"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

import {getSocket, connectToChunk} from "./socket"
import "./draggable"
import {addHandlers} from "./dragndrop"
import "../css/app.css";

export {getSocket, connectToChunk, addHandlers}
