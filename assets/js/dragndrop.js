// throws an error in the docker build, why?
import {Sortable} from "./draggable";

function addHandlers(channel) {
  const sortable = new Sortable(document.getElementById('chunk-elems'), {
    draggable: '.chunk-elem',
  });
  sortable.on('drag:over',
    (e) => e.data.over.classList.add("chunk-hover")
  );
  sortable.on('drag:out',
    (e) => e.data.over.classList.remove("chunk-hover")
  );
  sortable.on('sortable:stop',
    (e) => {
      // The library creates two extra temp elements we must remove
      let filteredChildren = Array.from(e.newContainer.children).filter(
        elem => !elem.classList.contains("draggable-mirror") && !elem.classList.contains("draggable--original")
      );
      // Parse the encodedValue to preserve type info
      let newList = filteredChildren.map(elem => JSON.parse(elem.dataset.encodedValue));
      channel.push("chunk_update", {"body": newList});
    }
  );
}

export {addHandlers}
