defmodule Crowdsort.ChunkTest do
  use Crowdsort.DataCase

  test "Move an element in a list" do
    lst = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert Chunk.move(lst, 9, 0) == [9, 0, 1, 2, 3, 4, 5, 6, 7, 8]
    assert Chunk.move(lst, 0, 9) == [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    assert Chunk.move(lst, 2, 7) == [0, 1, 3, 4, 5, 6, 7, 2, 8, 9]
    assert Chunk.move(lst, 4, 3) == [0, 1, 2, 4, 3, 5, 6, 7, 8, 9]
    assert Chunk.move(lst, 6, 6) == lst
    assert Chunk.move(lst, 3, 10) == [0, 1, 2, 4, 5, 6, 7, 8, 9, 3]
  end

  test "Calculate sortedness of a list" do
    # Test the simplest cases
    lst = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert Chunk.calc_sortedness(lst) == 1.0
    lst = [9, 8, 7, 6, 5, 4, 3, 2, 1]
    assert Chunk.calc_sortedness(lst) == 0.0
    # Test edge cases where there are multiple duplicates
    lst = ["unhappy", "enjoy", "enjoy", "unhappy", "reflection"]
    assert Chunk.calc_sortedness(lst) == 0.6
    lst = ["enjoy", "enjoy", "reflection", "unhappy", "unhappy"]
    assert Chunk.calc_sortedness(lst) == 1.0
    lst = ["enjoy", "enjoy", "unhappy", "unhappy"]
    assert Chunk.calc_sortedness(lst) == 1.0
  end
end
