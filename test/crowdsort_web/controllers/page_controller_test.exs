defmodule CrowdsortWeb.PageControllerTest do
  use CrowdsortWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Start sorting"
    assert html_response(conn, 200) =~ "Upload something to sort"
  end
end
