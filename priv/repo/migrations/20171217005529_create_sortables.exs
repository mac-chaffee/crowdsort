defmodule Crowdsort.Repo.Migrations.CreateSortables do
  use Ecto.Migration

  def change do
    create table(:sortables) do
      timestamps()
    end
  end
end
