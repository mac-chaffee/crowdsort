defmodule Crowdsort.Repo.Migrations.AddSortedField do
  use Ecto.Migration

  def change do
    alter table("chunks") do
      add :sorted, :boolean, default: false
    end
  end
end
