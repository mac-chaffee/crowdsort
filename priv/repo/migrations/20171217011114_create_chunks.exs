defmodule Crowdsort.Repo.Migrations.CreateChunks do
  use Ecto.Migration

  def change do
    create table(:chunks) do
      add :data, :map
      add :sortable_id, references(:sortables, on_delete: :delete_all)
      timestamps()
    end

    create index(:chunks, [:sortable_id])
  end
end
