# CrowdSort - A ~~satirical~~ revolutionary new sorting algorithm!

CrowdSort is a crowd-sourced sorting algorithm. Users of the site
can sort data for reward points (that don't mean anything), and other
users upload data to sort!

CrowdSort exclusively uses stereotypical startup technologies like
functional programming with elixir. Everything is severely pre-optimized
for when CrowdSort corners the sorting marking and leaves early access
in the year 2045.

## Why?

If you want to sort data, existing cloud compute solutions cost a lot of money.
But if you gamify it, you can get humans to do it for free!

And humans can work concurrently on the same problem to increase the speed
linearly (at least that's what my product manager thinks).

## How does CrowdSort work?
Let's say you want to track the progress of some data you've uploaded.
Here's what happens behind the scenes:

* Your browser requests the page `/track/{id}/`
* [Bandit](https://github.com/mtrudel/bandit) accepts the request
* [Phoenix](https://www.phoenixframework.org/) executes my code to render the HTML
* My code queries the [Postgres](https://www.postgresql.org/) database to get the chunk IDs of your uploaded data
* The rendered HTML (including the chunk IDs) is sent to your browser
* Your browser then executes some JavaScript (that was embedded in the HTML) which opens a [WebSocket](https://en.wikipedia.org/wiki/WebSocket) connection to my server
* Your browser uses the websocket connection to subscribe to messages on a specific topic via [Phoenix PubSub](https://hexdocs.pm/phoenix_pubsub/Phoenix.PubSub.html). In this case, you subscribe to one topic for every chunk
* Whenever a user connects to the same chunk's topic and whenever the order of elements in that chunk changes, your browser will receive a WebSocket message. That message then causes the UI (such as the progress bar) to update

------

## Development Environment Setup

* Install Docker and Elixir 1.17
* Install dependencies: `mix deps.get`
* Run a postgres db in Docker: `./run pg`
* Create and migrate your database: `mix ecto.create && mix ecto.migrate`
* Start the Phoenix server: `mix phx.server`

Then visit [`localhost:4000`](http://localhost:4000).

## Other administrative tasks

### Deploy

```
./run deploy

```

See crowdsort.service for the systemd configuration.
The `upgrade` script which runs on the VM is:

```
set -e
tar -xzf crowdsort.tar.gz
sudo systemctl restart crowdsort
sudo systemctl status crowdsort
```

### Create migrations

```
mix ecto.gen.migration migration_name
```

### Run migrations

```
bin/crowdsort eval "Crowdsort.Release.migrate"
bin/crowdsort eval "Crowdsort.Release.rollback(Crowdsort.Repo, 3)"
```

### Update Dependencies

```
mix deps.update --all
```

JS deps are vendored, so update those manually if necessary.

### Generate self-signed cert for testing HTTPS locally

```
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -nodes -subj '/CN=localhost'
```

### Renewing Certificate

I haven't yet set up automated renewal, so here's how to manually update:

```
sudo certbot renew --pre-hook="systemctl stop crowdsort" --post-hook="systemctl start crowdsort"
```
