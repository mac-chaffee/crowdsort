defmodule Crowdsort.Mixfile do
  use Mix.Project

  def project do
    [
      app: :crowdsort,
      version: "1.0.2",
      elixir: "~> 1.17",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Crowdsort.Application, []},
      extra_applications: [:logger, :runtime_tools, :ssl]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_),     do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.7.0"},
      {:phoenix_pubsub, ">= 2.1.3"},
      {:phoenix_ecto, ">= 4.6.2"},
      {:phoenix_view, "~> 2.0"},
      {:ecto_sql, ">= 3.11.3"},
      {:ecto_psql_extras, "~> 0.6"},
      {:esbuild, "~> 0.8", runtime: Mix.env() == :dev},
      {:postgrex, ">= 0.18.0"},
      {:phoenix_html, ">= 4.1.1"},
      {:phoenix_html_helpers, "~> 1.0"},
      {:phoenix_live_reload, ">= 1.5.3", only: :dev},
      {:phoenix_live_dashboard, "~> 0.8.3"},
      {:phoenix_live_view, "~> 0.20.17"},
      {:telemetry_metrics, "~> 1.0"},
      {:telemetry_poller, "~> 1.0"},
      {:gettext, ">= 0.24.0"},
      {:plug, ">= 1.16.1"},
      {:jason, "~> 1.4"},
      {:expletive, ">= 0.1.0"},
      {:bandit, "~> 1.5"},
      {:dns_cluster, "~> 0.1.3"},
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      "ecto.test": ["ecto.create --quiet", "ecto.migrate", "test"],
      "assets.setup": ["esbuild.install --if-missing"],
      "assets.build": ["esbuild crowdsort"],
      "assets.deploy": [
        "esbuild crowdsort --minify",
        "phx.digest"
      ]
    ]
  end
end
