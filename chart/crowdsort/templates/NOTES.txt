1. Open the application's port using this command:

   kubectl port-forward $(kubectl get pods | grep crowdsort-web | awk '{print $1}') 4000:4000
2. Then visit http://127.0.0.1:4000 to use your application
